# Keras
from tensorflow.keras.models import load_model, Model
from tensorflow.keras.preprocessing import image
# Scipy
import numpy as np
import matplotlib.pyplot as plt
# General
import string

# Settings
START_END_CHAR = '🍪'
CHARS = list(string.printable + START_END_CHAR) # Cookie for START/END char
NUM_CHARS = len(CHARS)
BEAM_WIDTH = 10

# Convert text to vectors
def text_to_vec(text):
	vecs = []
	for char in text:
		arr = np.zeros(NUM_CHARS)
		arr[CHARS.index(char)] = 1.
		vecs.append(arr)
	return np.array(vecs)

def load_img(path):
	img = image.load_img(path, target_size=(50, 100))
	return image.img_to_array(img) / 255.

def sample(img_path, model=None):
	if not model:
		model = load_model('captcha.h5')
	# model.summary()
	# attn_model = Model(model.inputs, model.get_layer('attention_output').output)
	img = load_img(img_path)
	beams = [{'text': START_END_CHAR, 'score': 0}]
	done = False
	i = 0
	while not done:
		for beam in list(beams):
			prediction = model.predict([[img], [text_to_vec(beam['text'])]])[0]
			# attn_pred = attn_model.predict([[img], [text_to_vec(beam['text'])]])[0]
			# print(attn_pred)
			# plt.imshow(attn_pred * 1000)
			# plt.show()
			for index in range(len(prediction)):
				next_char = CHARS[index]
				beams.append({
					'text': beam['text'] + next_char, 
					'score': beam['score'] + prediction[index],
				})
			beams.remove(beam)

		beams = sorted(beams, key=lambda x : x['score'], reverse=True)[:BEAM_WIDTH]

		if any([b['text'][-1] == START_END_CHAR for b in beams]):
			done = True
		elif i >= 20:
			done = True
		i += 1

	# print(beams)
	return beams[0]['text']

if __name__ == '__main__':
	print(sample('/media/kyle/HDD/Captchas/7mxdz.jpeg'))