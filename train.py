# Keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import (
	Input,
	Conv2D,
	MaxPooling2D,
	GlobalMaxPooling2D,
	GRU,
	Dense,
	Dropout,
	Concatenate,
	Flatten,
	Lambda,
	Reshape,
	RepeatVector,
)
from tensorflow.keras.preprocessing import image
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.optimizers import Adam
import tensorflow.keras.backend as K
# TensorFlow
import tensorflow as tf
# Scipy
import numpy as np
# Model Evaluation
from edit_distance import levenshteinDistance
from sample import sample
# General
import random
import string
import time
import os

# Settings
START_END_CHAR = '🍪'
CHARS = list(string.printable + START_END_CHAR) # Cookie for START/END char
NUM_CHARS = len(CHARS)
TRAIN_DATA_DIR = '/media/kyle/HDD/Captchas/train/'
TEST_DATA_DIR = '/media/kyle/HDD/Captchas/test/'
BATCH_SIZE = 128
STEPS_PER_EPOCH = 1000
EPOCHS = 100
FILTERS = 128

# Build the model
encoder_img_input = Input(shape=(50, 100, 3))
encoder_img = Conv2D(FILTERS, 3, activation='relu')(encoder_img_input)
encoder_img = MaxPooling2D(2)(encoder_img)
encoder_img = Dropout(0.1)(encoder_img)
encoder_img = Conv2D(FILTERS, 3, activation='relu')(encoder_img)
encoder_img = MaxPooling2D(2)(encoder_img)
encoder_img = Dropout(0.1)(encoder_img)
encoder_img = Conv2D(FILTERS, 3, activation='relu')(encoder_img)
# encoder_img = GlobalMaxPooling2D()(encoder_img)
encoder_img = Conv2D(FILTERS, 3, activation='relu', name='conv_output')(encoder_img)
encoder_img_output = Reshape((
	K.int_shape(encoder_img)[3],
	K.int_shape(encoder_img)[1] * K.int_shape(encoder_img)[2],
))(encoder_img)
# encoder_img_output = Flatten()(encoder_img)
# encoder_img_output = Dense(128, activation='relu')(encoder_img)

encoder_text_input = Input(shape=(None, NUM_CHARS))
encoder_text = GRU(32, return_sequences=True)(encoder_text_input)
encoder_text = Dropout(0.1)(encoder_text)
encoder_text = GRU(32)(encoder_text)
encoder_text = Dropout(0.1)(encoder_text)
encoder_text_output = RepeatVector(FILTERS)(encoder_text)

attention = Concatenate()([encoder_img_output, encoder_text_output])
attention = Dense(K.int_shape(encoder_img_output)[2], 
	activation='softmax', name='attention_weights')(attention)
attention = Lambda(lambda x : x[0] * x[1], 
	name='attention_weighting')([encoder_img_output, attention])
attention = Reshape((
	K.int_shape(encoder_img)[1], 
	K.int_shape(encoder_img)[2],
	K.int_shape(encoder_img)[3],
), name='attention_output')(attention)

output = Conv2D(FILTERS, 2)(attention)
output = Dropout(0.3)(output)
output = Conv2D(FILTERS, 2)(output)
output = Dropout(0.3)(output)
output = Conv2D(8, 2)(output)
output = Flatten()(output)
# output = GlobalMaxPooling2D()(output)
output = Dropout(0.3)(output)
output = Dense(NUM_CHARS, activation='softmax', name='output')(output)

model = Model([encoder_img_input, encoder_text_input], output)
opt = Adam(clipnorm=5)
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

model.summary()

# Convert text to vectors
def text_to_vec(text):
	vecs = []
	for char in text:
		arr = np.zeros(NUM_CHARS)
		arr[CHARS.index(char)] = 1.
		vecs.append(arr)
	return np.array(vecs)
# Load the data
def load_data():
	images = os.listdir(TRAIN_DATA_DIR)
	while True:
		x1s, x2s, ys = {}, {}, {}
		random.shuffle(images)
		for img_fn in images:
			img = image.load_img(TRAIN_DATA_DIR + img_fn, target_size=(50, 100))
			img = image.img_to_array(img) / 255.
			# Get text with start and end char
			text = START_END_CHAR + img_fn.split('.')[0] + START_END_CHAR
			for i in range(len(text)):
				if i > 0:
					x2 = text_to_vec(text[:i])
					y = text_to_vec(text[i])
					try:
						x1s[i].append(img)
						x2s[i].append(x2)
						ys[i].append(y[0])
					except KeyError:
						x1s[i] = [img]
						x2s[i] = [x2]
						ys[i] = [y[0]]
					
					if len(x1s[i]) >= BATCH_SIZE:
						yield [np.array(x1s[i]), np.array(x2s[i])], np.array(ys[i])
						del(x1s[i])
						del(x2s[i])
						del(ys[i])

writer = tf.summary.create_file_writer('metrics/{}/'.format(int(time.time())))
writer.set_as_default()
def edit_distance_loss(model):
	images = os.listdir(TEST_DATA_DIR)
	random.shuffle(images)
	distances = []
	for i, img_path in enumerate(images[:50]):
		real_text = img_path.split('.')[0]
		guess_text = sample(TEST_DATA_DIR + img_path, model=model).replace(START_END_CHAR, '')
		if i % 10 == 0:
			print('Real: {}, Predicted: {}'.format(real_text, guess_text))
		distances.append(levenshteinDistance(real_text, guess_text))
	return np.mean(distances)

def train():
	try:
		model.load_weights('captcha.h5')
	except Exception as e:
		print(e)

	for epoch in range(EPOCHS):
		model.fit_generator(
			load_data(),
			steps_per_epoch=STEPS_PER_EPOCH,
			epochs=1,
			callbacks=[
				ModelCheckpoint(
					filepath='captcha.h5', 
					load_weights_on_restart=True, 
					save_format='h5'
				)
			]
		)

		tf.summary.scalar('edit distance', edit_distance_loss(model), step=epoch * STEPS_PER_EPOCH)

if __name__ == '__main__':
	train()